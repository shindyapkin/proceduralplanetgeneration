using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*!
    @class CheckBox_Panel_Handler : MonoBehaviour
    @brief ������, ������� ������������ ���� � checkBox
*/
public class CheckBox_Panel_Handler : MonoBehaviour
{
    /*!
        @var Toggle _toggle
        @brief ��������� ����������, � ������� ������ ���� �������� �������������� checkBox
    */
    public Toggle _toggle;

    /*!
        @var bool _is_On
        @brief ����������, ������� �������� � ���� ��������� checkBox
    */
    bool _is_On = false;


    /*!
        @fn void Start()
        @brief ����������� ����� MonoBehaviour, ����������� ��� � ������ ������ �������
        @details ����� �������� ������� �������� checkBox'�
    */
    void Start()
    {
        _is_On = _toggle.isOn;
    }

    /*!
        @fn public void onToggled()
        @brief ����� ������������� ��� ������������ checkBox
        @details ����� �������� ������� �������� checkBox'�
    */
    public void onToggled()
    {
        _is_On = _toggle.isOn;
    }

    /*!
        @fn public bool getValue()
        @brief ����� ������������� ��� ������������ checkBox
        @details ����� �������� ������� �������� checkBox'�
        @return ���������� ������� �������� �����������
    */
    public bool getValue()
    {
        return _is_On;
    }
}
