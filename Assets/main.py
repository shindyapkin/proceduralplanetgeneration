
def get_song_name(song_path):
    song_name = song_path.split(sep='/')[-1]
    if song_name[-5] == '.':
        song_name = song_name[:-5]
    else:
        song_name= song_name[:-4]
    return song_name

def main():

    try:

        import mongo_connector

        import music_processing
        import os,json
        resolution = input()
        save = input()
        song_path = input()
        short_name = get_song_name(song_path)

        collection = mongo_connector.create_session()

        mesh  = list(mongo_connector.find_mesh(collection, short_name, resolution))
        if not mesh:
            song_name = music_processing.change_to_wav(song_path)
            music_processing.convert_to_mono(song_name)
            #music_processing.load_song(song_name)
            mesh = music_processing.create_mesh(song_name,int(resolution)).T.flatten().tolist()
            if save == 'True':
                mongo_connector.save_mesh(collection, {"name": short_name, "resolution": resolution, "mesh" : mesh})
            #mesh = mongo_connector.find_mesh(collection, short_name, resolution).next()['mesh']
        else:
            mesh = mesh[0]['mesh']
        with open( './Music/song_mesh.json', 'w') as f: # os.path.dirname(__file__) +
            json.dump({'mesh': mesh}, f)
    except Exception as e:
          print('Никитос, всё плохо, у нас:' + '\n')
          raise
main()