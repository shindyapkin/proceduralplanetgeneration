using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.IO;
using System.Diagnostics;


/*!
    @class NoiseCreator
    @brief �����, �������������� ���� ������� �������
*/
public class NoiseCreator
{


    /*!
        @class HSV
        @brief �����, �������������� �� ���� ��������� ����� � ��������
    */
    private class HSV
    {
        public float[] mesh;
    }
    [SerializeField] private HSV hsv;


    /*!
        @fn public float[,] createNoiseMap(int horizontal_len, int vertical_len, string filePath, bool isSavePlanet)
        @brief �����, ��������� ������ ������������
        @details ��������� .py ������ � �������� �� ���� ������ 
        @param horizontal_len[IN] ���������� ������ �� �����������
        @param vertical_len[IN] ���������� ������ �� ���������
        @param filePath[IN] ���� � ���������� + ��� �������� � �����
        @param isSavePlanet[IN] ��������, ������������ ����� �� ��������� � �� ��������� ������
        @return ���������� ������ � �������������� ����� ��� ������
    */
    public float[,] createNoiseMap(int horizontal_len, int vertical_len, string filePath, bool isSavePlanet)
    {
        float[,] shape = new float[horizontal_len, vertical_len];
        hsv = new HSV();
        string t = pyScriptStart(horizontal_len.ToString(), vertical_len.ToString(), filePath, isSavePlanet);
        string curFile = Application.dataPath + "/Music/song_mesh.json";
        if (File.Exists(curFile))
        {
            string file_json = File.ReadAllText(curFile);
            HSV json = JsonUtility.FromJson<HSV>(file_json);
            for (int a = 0; a < (horizontal_len); a++)
            {
                for (int b = 0; b < (vertical_len); b++) 
                {
                    if (a != (horizontal_len - 1))   
                    {
                        shape[a, b] = json.mesh[(a * vertical_len) + b]; 
                    }
                    else
                    {
                        shape[a, b] = shape[0, b];
                    }
                }
            }
        } 
        else
        {
            return createNoShape(horizontal_len, vertical_len);
        }
        return shape;
    }

    /*!
        @fn public float[,] createNoShape(int horizontal_len, int vertical_len)
        @brief �����, ��������� ������ ������������
        @details ������� ������ � ��������� ������������� (��� �������� ������� ����� 1)
        @param horizontal_len[IN] ���������� ������ �� �����������
        @param vertical_len[IN] ���������� ������ �� ���������
        @return ���������� ������ � �������������� ����� ��� ������
    */
    public float[,] createNoShape(int horizontal_len, int vertical_len)
    {
        float[,] shape = new float[horizontal_len, vertical_len];
        for (int x = 0; x < horizontal_len; x++)
        {
            for (int y = 0; y < vertical_len; y++)
            {
                shape[x, y] = 1f;
            }
        }
        return shape;
    }

    /*!
        @fn string pyScriptStart(string horizonatl_len, string vertical_len, string filePath, bool isSavePlanet)
        @brief �����, ����������� ������ �������� �������
        @details ��������� .py ������ � �������� �� ���� ������ 
        @param horizontal_len[IN] ���������� ������ �� �����������
        @param vertical_len[IN] ���������� ������ �� ���������
        @param filePath[IN] ���� � ���������� + ��� �������� � �����
        @param isSavePlanet[IN] ��������, ������������ ����� �� ��������� � �� ��������� ������
        @return ���������� ������ ������ � ����������� ������
    */
    string pyScriptStart(string horizonatl_len, string vertical_len, string filePath, bool isSavePlanet)
    {
        string output = null;
        using (Process process = new Process())
        {
            process.StartInfo.FileName = "cmd.exe";
            process.StartInfo.Arguments = @"/c python " + Application.dataPath + @"\main.py";
            process.StartInfo.WorkingDirectory = Application.dataPath;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardInput = true;
            
            process.Start();
            
            process.StandardInput.WriteLine(vertical_len);
            process.StandardInput.WriteLine(isSavePlanet);
            process.StandardInput.WriteLine(filePath);
            string stdoutx = process.StandardOutput.ReadToEnd();
            string stderrx = process.StandardError.ReadToEnd();
            
            //print(stdoutx);
            //print(stderrx);
            output = process.StandardOutput.ReadLine();
            process.Close();
        }
        return output;
    }
}
