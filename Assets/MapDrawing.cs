using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*!
    @class MapDrawing : MonoBehaviour
    @brief ������, ���������� �� ��������� ����� �������
*/
public class MapDrawing : MonoBehaviour
{
    /*!
        @var RawImage _map
        @brief ������, � �������� ������ ���� �������� �������������� RawImage
    */
    RawImage _map;
    /*!
        @var Gradient _map_gradient
        @brief ������, ���������� �� �������� �������� �����
    */
    Gradient _map_gradient;
    /*!
        @var _width
        @brief ������ RawImage � ��������
    */
    int _width;
    /*!
        @var _height
        @brief ������ RawImage � ��������
    */
    int _height;

    /*!
        @fn void Start()
        @brief ����������� ����� MonoBehaviour, ����������� ��� � ������ ������ �������
        @details ������������� ��������� _width � _height ������������ RawImage
    */
    void Start()
    {
        _map = GetComponent<RawImage>();
        _width = (int)_map.rectTransform.rect.width;
        _height = (int)_map.rectTransform.rect.height;
    }

    /*!
        @fn public void setMap(int x_size, int y_size, float[,] shape, bool color_map)
        @brief �����, ��������� ����� ����������� �������
        @details ������ ������� ������������ ������� RawImage � ������� ������� shape
        @param x_size[IN] ������ �� �����������
        @param y_size[IN] ������ �� ���������
        @param float[,] shape[IN] ������ �������
        @param bool color_map[IN] ��������, ���������� �� ��, ������������ �� �������� ��������
    */
    public void setMap(int x_size, int y_size, float[,] shape, bool color_map)
    {
        var texture = new Texture2D(_width, _height);
        float x_step = (float)x_size / (float)_width;
        float y_step = (float)y_size / (float)_height;

        for (int x = 0; x < _width; x++)
        {
            for (int y = 0; y < _height; y++)
            {
                int cur_x = (int)((float)x * x_step);
                int cur_y = (int)((float)y * y_step);
                Color color;
                if (color_map)
                {
                    color = _map_gradient.Evaluate(shape[cur_x, cur_y] - 0.2f);
                } else
                {
                    color = _map_gradient.Evaluate(0.2f);
                }
                texture.SetPixel(x, _height - y, color);
            }
        }
        texture.Apply();
        _map.texture = texture;
    }

    /*!
        @fn public void setGradient(Gradient gradient)
        @brief �����, ��������������� �������� �����
        @details ������ _map_gradient ����� ��������
        @param gradient[IN] ����� �������� �����
    */
    public void setGradient(Gradient gradient)
    {
        _map_gradient = gradient;
    }
}
