using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SongModel
{
    public string Stringify()

    {

        return JsonUtility.ToJson(this);

    }

    public static SongModel Parse(string json)

    {

        return JsonUtility.FromJson<SongModel>(json);

    }



}
