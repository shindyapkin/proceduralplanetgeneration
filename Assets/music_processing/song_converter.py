

def change_to_wav(song_path):
    import string
    import soundfile as sf
    song_name = song_path.split(sep='/')[-1]
    if song_name[-5] == '.':
        song1 = song_name[:-5]
    else:
        song1 = song_name[:-4]
    save_song = '{}{}'.format(song1, '.wav')
    song, rate = sf.read('./Music/{}'.format(song_name))
    sf.write('./Music/{}'.format(save_song), song, rate)
    return save_song

def load_song(song_name):
    import soundfile as sf
    song, rate = sf.read('./Music/{}'.format(song_name))
    return (song,rate)



def convert_to_mono(song_name):
    from pydub import AudioSegment

    # song_name = 'Love Street.wav'

    sound = AudioSegment.from_file('./Music/{}'.format(song_name))
    sound = sound.set_channels(1)
    sound.export('./Music/{}'.format(song_name), format="wav")
    #return load_song('./Music/{}'.format(song_name))

