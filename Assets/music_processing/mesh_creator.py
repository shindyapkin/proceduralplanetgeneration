
def create_spectogram(_wav_file_):
    import librosa
    from librosa.display import specshow
    import matplotlib.pyplot as plt
    import numpy as np
    import soundfile as sf


    (sig, rate) = sf.read(_wav_file_) #, sr=None, mono=True,  dtype=np.float32)

    S = librosa.feature.melspectrogram(sig, n_fft=4096, hop_length=1024)
    fig, ax = plt.subplots()
    fig.set_figheight(15)
    fig.set_figwidth(40)
    S_dB = librosa.power_to_db(S, ref=np.max)
    img = librosa.display.specshow(S_dB,sr=rate,

                         fmax=8000, ax=ax)
    plt.savefig('./Music/melspec.png', dpi = 100)


def create_mesh(song_name, resolution):
    wav_file = './Music/{}'.format(song_name) #'./Music/{}.wav'.format(song_name)
    create_spectogram(wav_file)
    map = create_map(resolution)
    import os
    os.remove('./Music/melspec.png')
    return map

def create_map(resolution):
    import cv2 as cv
    from sklearn.preprocessing import MinMaxScaler
    imspec = cv.imread('./Music/melspec.png')
    mm = MinMaxScaler((0.85,1.1))
    #print(imspec.shape[0],imspec.shape[1])
    x_min =  imspec.shape[0] //2# 750
    y_min = 1000  #2010
    x_max = x_min+resolution
    y_max = y_min+resolution*2  #-10
    gray = cv.cvtColor(imspec[x_min:x_max, y_min:y_max], cv.COLOR_BGR2GRAY)
    border_gray = cv.copyMakeBorder(gray, 0, 0, 10, 10, cv.BORDER_WRAP)
    blur = cv.bilateralFilter(border_gray, 60, 300, 75)
    mm_gray = mm.fit_transform(blur)
    return mm_gray