using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

/*!
    @class UI_Handler : MonoBehaviour
    @brief ������, ���������� �� ������ � ���������������� ����������
*/
public class UI_Handler : MonoBehaviour
{
    /*!
        @var public Image _size_panel
        @brief ��������� ����������, � ������� ������ ���� ��������� ������ ������� �������
    */
    public Image _size_panel;
    /*!
        @var public Image _resolution_panel
        @brief ��������� ����������, � ������� ������ ���� ��������� ������ ���������� �������
    */
    public Image _resolution_panel;
    /*!
        @var public Image _filePath_panel
        @brief ��������� ����������, � ������� ������ ���� ��������� ������ ���� � ����������
    */
    public Image _filePath_panel;
    /*!
        @var public Image _checkbox_panel
        @brief ��������� ����������, � ������� ������ ���� ��������� ������ ������������� �������� �������
    */
    public Image _checkbox_panel;
    /*!
        @var public Image _save_planet_checkbox
        @brief ��������� ����������, � ������� ������ ���� ��������� ������ ���������� �������
    */
    public Image _save_planet_checkbox;
    /*!
        @var RawImage _map_image
        @brief ������, � �������� ������ ���� �������� �������������� RawImage
    */
    public RawImage _map_image;
    /*!
        @var GameObject _object
        @brief ������ �������
    */
    GameObject _object;
    /*!
        @var NoiseCreator _noise
        @brief ������ ������� �������
    */
    NoiseCreator _noise;
    /*!
        @var Planet _planet
        @brief ������ �������
    */
    Planet _planet;
    /*!
        @var MapDrawing _map
        @brief ������ �����
    */
    MapDrawing _map;
    /*!
        @var Gradient _map_gradient
        @brief �������� �������� �������
    */
    Gradient _map_gradient;

    /*!
        @fn void Start()
        @brief ����������� ����� MonoBehaviour, ����������� ��� � ������ ������ �������
        @details ����� ������ ������� ������� � �����, � ����� ������ �� �������� �������
    */
    void Start()
    {
        createPlanetGameObject(_object);

        _map = _map_image.GetComponent<MapDrawing>();

        _noise = new NoiseCreator();

        _map_gradient = new Gradient();
        var colorKey = new GradientColorKey[6];
        colorKey[0].color = new Color(66f / 255f, 83f / 255f, 255f / 255f);
        colorKey[0].time = 0.65f;
        colorKey[1].color = new Color(63f / 255f, 217f / 255f, 255f / 255f);
        colorKey[1].time = 0.7f;
        colorKey[2].color = new Color(253f / 255f, 223f / 255f, 68f / 255f);
        colorKey[2].time = 0.75f;
        colorKey[3].color = new Color(16f / 255f, 192f / 255f, 32f / 255f);
        colorKey[3].time = 0.85f;
        colorKey[4].color = new Color(195f / 255f, 127f / 255f, 32f / 255f);
        colorKey[4].time = 0.95f;
        colorKey[5].color = new Color(106f / 255f, 54f / 255f, 7f / 255f);
        colorKey[5].time = 1.0f;
        _map_gradient.colorKeys = colorKey;

        int size = getSize();
        int resolution = getResolution();
        bool isColorPalette = getIsColorPalette();
        int x_body_size = 2 * resolution;
        int y_body_size = resolution;
    }

    /*!
        @fn void Update()
        @brief ����������� ����� MonoBehaviour, ����������� ��� ������ ����� �����
        @details ����� ����������� ������� � �������� ���� ��� �������� �������
    */
    void Update()
    {
        if (Input.GetMouseButton(0) && Input.GetMouseButton(1))
        {
            _planet.transform.rotation = new Quaternion(0, 0, 0, 0);
        } else
        if (Input.GetMouseButton(1))
        {
            Vector3 mousePos = Input.mousePosition;
            float RotationSpeed = 2500;
            _planet.transform.Rotate((Input.GetAxis("Mouse Y") * RotationSpeed * Time.deltaTime), (Input.GetAxis("Mouse X") * RotationSpeed * Time.deltaTime * (-1)), 0, Space.World);
        }
    }

    /*!
        @fn void createPlanetGameObject(GameObject obj)
        @brief ����� ��� �������� ������� �������
        @details ������� ������ �������, ����������� ��� Planet, MeshFilter, MeshRenderer, � ����� ������ ��� ���������
        @param obj[IN] ������� ������ GameObject
    */
    void createPlanetGameObject(GameObject obj)
    {
        obj = new GameObject("PlanetObj");
        obj.AddComponent<Planet>();
        obj.AddComponent<MeshFilter>();
        obj.AddComponent<MeshRenderer>();
        obj.transform.localPosition = new Vector3(0, 0, 20);
        _planet = obj.GetComponent<Planet>();
    }

    /*!
        @fn public void generatePlanet()
        @brief ����� ��� ��������� ����������� �������
        @details �������� �� ������� ���������� �������� � ���������� ����������� �������
    */
    public void generatePlanet()
    {
        int size = getSize();
        int resolution = getResolution();
        bool isColorPalette = getIsColorPalette();
        bool isSavePlanet = getIsSavePlanet();

        _planet.setBodySize(size);

        int x_body_size = 2 * resolution;
        int y_body_size = resolution;

        float[,] shape = _noise.createNoiseMap(x_body_size, y_body_size, getMusicFilePath(), isSavePlanet);

        _planet.setGradient(_map_gradient);
        _map.setGradient(_map_gradient);
        _planet.CreatePlanet(x_body_size, y_body_size, shape, isColorPalette);
        _map.setMap(x_body_size, y_body_size, shape, isColorPalette);
    }

    /*!
        @fn string getMusicFilePath()
        @brief ����� ��� ��������� �������� �� ������ ���� ����������
        @return ���� � ����������
    */
    string getMusicFilePath()
    {
        return _filePath_panel.GetComponent<Path_PanelHandler>().getPath();
    }

    /*!
        @fn int getSize()
        @brief ����� ��� ��������� �������� �� ������ ������� �������
        @return ������ �������
    */
    int getSize()
    {
        return _size_panel.GetComponent<TSE_CellHanler>().getValue();
    }

    /*!
        @fn int getResolution()
        @brief ����� ��� ��������� �������� �� ������ ���������� �������
        @return ���������� �������
    */
    int getResolution()
    {
       return _resolution_panel.GetComponent<TSE_CellHanler>().getValue();
    }

    /*!
        @fn bool getIsColorPalette()
        @brief ����� ��� ��������� �������� �� ������ ������������� �������� �������
        @return true - ������������ �������� �������, false - �� ������������
    */
    bool getIsColorPalette()
    {
        return _checkbox_panel.GetComponent<CheckBox_Panel_Handler>().getValue();
    }

    /*!
        @fn bool getIsSavePlanet()
        @brief ����� ��� ��������� �������� �� ������ ���������� �������
        @return true - ���������, false - �� ���������
    */
    bool getIsSavePlanet()
    {
        return _save_planet_checkbox.GetComponent<CheckBox_Panel_Handler>().getValue();
    }
}
