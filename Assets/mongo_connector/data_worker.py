from .config import conn_str


def create_session():
    import pymongo
    client = pymongo.MongoClient(conn_str, serverSelectionTimeoutMS=5000)
    db = client.Planets
    collection = db.Planet
    return collection

def save_mesh(collection,params):
    collection.insert_one(params)

def find_mesh(collection, song_name, resolution):
    return collection.find({"name": song_name, "resolution" : resolution})








