using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*!
    @class Path_PanelHandler
    @brief �����, ���������� �� ���������� ���������� ���������
*/
public class PolygonBuilder
{
    /*!
        @var List<Vector3> _Vertices
        @brief ������ �� ����� ����������� �������
    */
    private List<Vector3> _Vertices = new List<Vector3>();
    /*!
        @var List<int> _Triangles
        @brief ������ �� ������������������ ������� ����� ��� ���������� ���������
    */
    private List<int> _Triangles = new List<int>();
    /*!
        @var List<Color> _Colors
        @brief ������ �� ������ ����� ����������� �������
    */
    private List<Color> _Colors = new List<Color>();

    /*!
        @fn public void AddTrianglePolygon(Vector3 first_dot, Vector3 second_dot, Vector3 third_dot, bool both_sides)
        @brief ����� ��� �������� ��������
        @details ������� ������� �� ������ ���� ����� � ������������
        @param first_dot[IN] ������ �����
        @param second_dot[IN] ������ �����
        @param third_dot[IN] ������ �����
        @param both_sides[IN] ����������, ����������, ����� �� �������� ������� � ����� ������� (false) ��� � ���� (true)
    */
    public void AddTrianglePolygon(Vector3 first_dot, Vector3 second_dot, Vector3 third_dot, bool both_sides)
    {
        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(first_dot);
        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(second_dot);
        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(third_dot);

        if (both_sides)
        {
            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(third_dot);
            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(second_dot);
            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(first_dot);
        }
    }

    /*!
        @fn public void AddSquarePolygon(Vector3 first_dot, Vector3 second_dot, Vector3 third_dot, Vector3 fourth_dot, bool both_sides)
        @brief ����� ��� �������� �������������� �� ���� ���������
        @details ������� ������������� �� ������ ������� ����� � ������������
        | fouth_dot | ------ | third_dot |
        |           |        |           |
        |-----------|        |-----------|
        |           |        |           |
        | first_dot | ------ |second_dot |
        @param first_dot[IN] ������ �����
        @param second_dot[IN] ������ �����
        @param third_dot[IN] ������ �����
        @param fourth_dot[IN] ��������� �����
        @param both_sides[IN] ����������, ����������, ����� �� �������� ������� � ����� ������� (false) ��� � ���� (true)
    */
    public void AddSquarePolygon(Vector3 first_dot, Vector3 second_dot, Vector3 third_dot, Vector3 fourth_dot, bool both_sides)
    {
        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(first_dot);
        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(second_dot);
        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(third_dot);

        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(third_dot);
        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(second_dot);
        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(fourth_dot);

        if (both_sides)
        {
            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(third_dot);
            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(second_dot);
            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(first_dot);

            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(fourth_dot);
            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(second_dot);
            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(third_dot);
        }
    }

    /*!
        @fn public void AddSquarePolygon(Vector3 first_dot, Vector3 second_dot, Vector3 third_dot, Vector3 fourth_dot, bool both_sides)
        @brief ����� ��� �������� �������� �������������� �� ���� ���������
        @details ������� ������������� �� ������ ������� ����� � ������������
        | fouth_dot | ------ | third_dot |
        |           |        |           |
        |-----------|        |-----------|
        |           |        |           |
        | first_dot | ------ |second_dot |
        @param first_dot[IN] ������ �����
        @param second_dot[IN] ������ �����
        @param third_dot[IN] ������ �����
        @param fourth_dot[IN] ��������� �����
        @param color[IN] ����� ������
        @param both_sides[IN] ����������, ����������, ����� �� �������� ������� � ����� ������� (false) ��� � ���� (true)
    */
    public void AddSquarePolygonWithColor(Vector3 first_dot, Vector3 second_dot, Vector3 third_dot, Vector3 fourth_dot, Color[] color, bool both_sides)
    {
        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(first_dot);
        _Colors.Add(color[0]);
        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(second_dot);
        _Colors.Add(color[1]);
        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(third_dot);
        _Colors.Add(color[2]);

        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(third_dot);
        _Colors.Add(color[2]);
        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(second_dot);
        _Colors.Add(color[1]);
        _Triangles.Add(_Vertices.Count);
        _Vertices.Add(fourth_dot);
        _Colors.Add(color[3]);

        if (both_sides)
        {
            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(third_dot);
            _Colors.Add(color[2]);
            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(second_dot);
            _Colors.Add(color[1]);
            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(first_dot);
            _Colors.Add(color[0]);

            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(fourth_dot);
            _Colors.Add(color[3]);
            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(second_dot);
            _Colors.Add(color[1]);
            _Triangles.Add(_Vertices.Count);
            _Vertices.Add(third_dot);
            _Colors.Add(color[2]);
        }
    }

    /*!
        @fn public void ClearPolygons()
        @brief �����, ����������� ��� ������� ���������
        @details ������� ������� _Vertices, _Triangles, _Colors
    */
    public void ClearPolygons()
    {
        _Vertices.Clear();
        _Triangles.Clear();
        _Colors.Clear();
    }

    /*!
        @fn public List<Vector3> getVertices()
        @brief �����, ������������ ������ ������
        @return ������ ������
    */
    public List<Vector3> getVertices()
    {
        return _Vertices;
    }

    /*!
        @fn public List<int> getTriangles()
        @brief �����, ������������ ������ ������� ������
        @return ������ ������� ������
    */
    public List<int> getTriangles()
    {
        return _Triangles;
    }

    /*!
        @fn public List<Color> getColors()
        @brief �����, ������������ ������ ������ ������
        @return ������ ������ ������
    */
    public List<Color> getColors()
    {
        return _Colors;
    }
}
