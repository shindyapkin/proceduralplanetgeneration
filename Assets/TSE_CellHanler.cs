using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*!
    @class TSE_CellHanler : MonoBehaviour
    @brief ������, ������� ������������ ���� � ������ ���������� ��� Text, Slider, Edit
*/
public class TSE_CellHanler : MonoBehaviour
{
    /*!
        @var Slider _slider
        @brief ��������� ����������, � ������� ������ ���� �������� �������������� Slider
    */
    public Slider _slider;
    /*!
        @var Slider InputField _edit
        @brief ��������� ����������, � ������� ������ ���� �������� �������������� InputField
    */
    public InputField _edit;

    /*!
        @var int _min_value
        @brief ��������� ����������, ������� ������ ����������� �������� ��� ����� InputField � Slider
    */
    public int _min_value;
    /*!
        @var int _max_value
        @brief ��������� ����������, ������� ������ ������������ �������� ��� ����� InputField � Slider
    */
    public int _max_value;

    /*!
        @var int _current_val
        @brief ��������� ����������, ������� �������� ������� �������� ����� InputField � Slider
    */
    int _current_val;

    /*!
        @fn void Start()
        @brief ����������� ����� MonoBehaviour, ����������� ��� � ������ ������ �������
        @details ����� ������ ��������� ��������� ����� � �������� ��������
    */
    private void Start()
    {
        //������������� �������� �������� ������������� ����� �����
        _slider.wholeNumbers = true;

        //������������� ����������� � ������������ ��������
        _slider.minValue = _min_value;
        _slider.maxValue = _max_value;
        _slider.value = _min_value;

        //Edit �� ����� ���� ������ float, ������� �������� ������ � int
        _edit.contentType = InputField.ContentType.IntegerNumber;
        _edit.text = _min_value.ToString();

        //������� �������� � ��� Edir'a, � ��� ��������
        _current_val = _min_value;
    }

    /*!
        @fn public void onSliderMoved()
        @brief �����, ������������� ��� �������� �������� Slider
        @details ������ ����� InputField'a ���� �������� �������� Slider
    */
    public void onSliderMoved()
    {
        _current_val = (int)_slider.value;
        _edit.text = _current_val.ToString();

    }

    /*!
        @fn public void onTextEdited()
        @brief �����, ������������� ��� �������� ������ � InputField
        @details ������ ��������� Slider ���� �������� �������� InputField'a
    */
    public void onTextEdited()
    {
        _slider.value = int.Parse(_edit.text);
        _current_val = (int)_slider.value;
    }

    /*!
        @fn public int getValue()
        @brief ����� ��������� �������� �� �������
        @return ���������� ������� �������� ����� InputField � Slider
    */
    public int getValue()
    {
        return _current_val;
    }
}
