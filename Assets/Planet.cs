using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*!
    @class Planet : MonoBehaviour
    @brief ������, ���������� �� ��������� �������
*/
public class Planet : MonoBehaviour
{
    /*!
        @var int _body_size
        @brief ����������, �������� � ���� ������ ������� 
    */
    int _body_size = 20;

    /*!
        @var Mesh _mesh
        @brief ����������, �������� mesh �������
    */
    Mesh _mesh;

    /*!
        @var PolygonBuilder _polygonBuilder
        @brief ������, ���������� �� ��������� ���������
    */
    PolygonBuilder _polygonBuilder;
    /*!
        @var Gradient _map_gradient
        @brief ������, ���������� �� �������� �������� �������
    */
    Gradient _map_gradient;
    /*!
        @var bool filled
        @brief ����������, ����������, �������� �� _mesh (���������� ��� ������� ����)
    */
    bool filled = false;

    /*!
        @fn void Start()
        @brief ����������� ����� MonoBehaviour, ����������� ��� � ������ ������ �������
        @details ����� ������ ��������� ��������� �������
    */
    void Start()
    {
        _mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = _mesh;
        //2 ��������� �����, �.�. ������ � Sprites/Default ������������ ����� � � ���������� �������
        Material[] materials = new Material[2];
        materials[0] = new Material(Shader.Find("Standard"));
        materials[1] = new Material(Shader.Find("Sprites/Default"));
        GetComponent<MeshRenderer>().materials = materials;
        //����������� ������ mesh � unity �� ������������ ������ 65535 ������, ������� ������ ������ ������� �������
        _mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
        _polygonBuilder = new PolygonBuilder();
        _map_gradient = new Gradient();
    }

    /*!
        @fn void BuildBody()
        @brief �����, ����������� ��� ���������� ����
        @details �������� �� _polygonBuilder ������� vertices, triangles � colors
    */
    void BuildBody()
    {
        //��������� ���� �������� � ����
        _mesh.vertices = _polygonBuilder.getVertices().ToArray();
        _mesh.triangles = _polygonBuilder.getTriangles().ToArray();
        _mesh.colors = _polygonBuilder.getColors().ToArray();
        filled = true;
    }

    /*!
        @fn void ClearBody()
        @brief �����, ����������� ��� ������� ����
        @details ���� filled == true, �� mesh ��������, ���� flase - �� ������ �� ����������
    */
    void ClearBody()
    {
        if (filled)
        {
            _mesh.Clear();
            _polygonBuilder.ClearPolygons();
            filled = false;
        }   
    }

    /*!
        @fn void MakeMesh(int x_body_size, int y_body_size, float[,] shape, bool color_map)
        @brief �����, ��������� ����������� �������
        @details ���� ���������� ������ � �� �� ������ ������ ��������
        @param x_body_size[IN] ���������� ������ �� �����������
        @param y_body_size[IN] ���������� ������ �� ���������
        @param float[,] shape[IN] ������ ������������� ������ ������ (������)
        @param bool color_map[IN] ��������, ���������� �� ��, ������������ �� �������� ��������
    */
    void MakeMesh(int x_body_size, int y_body_size, float[,] shape, bool color_map)
    {
        float delta = (float)_body_size / 2.0f;
        bool bothSides = false;

        Vector3[,] body_coords = new Vector3[x_body_size, y_body_size];
        for (int j = 0; j < y_body_size; j++)
        {
            for (int i = 0; i < x_body_size; i++)
            {
                //��������������� ��������� �����
                float O = Mathf.PI * (float)(j) / (float)(y_body_size - 1.0f);
                float F = 2.0f * Mathf.PI * (float)(i) / (float)(x_body_size - 1.0f);
                body_coords[i, j].x = delta * Mathf.Sin(O) * Mathf.Cos(F);
                body_coords[i, j].y = delta * Mathf.Cos(O);
                body_coords[i, j].z = delta * Mathf.Sin(O) * Mathf.Sin(F);
            }
        }

        //��������� �������� �� �����
        for (int x = 0; x < (x_body_size - 1); x++)
        {
            for (int y = 0; y < (y_body_size - 1); y++)
            {
                Color[] colors = new Color[4];
                if (color_map)
                {
                    //����������� �����, ���������� �� ����� �������
                    colors[0] = _map_gradient.Evaluate(shape[x, y] - 0.2f);
                    colors[1] = _map_gradient.Evaluate(shape[x + 1, y] - 0.2f);
                    colors[2] = _map_gradient.Evaluate(shape[x, y + 1] - 0.2f);
                    colors[3] = _map_gradient.Evaluate(shape[x + 1, y + 1] - 0.2f);
                }
                else
                {
                    colors[0] = _map_gradient.Evaluate(0.2f);
                    colors[1] = _map_gradient.Evaluate(0.2f);
                    colors[2] = _map_gradient.Evaluate(0.2f);
                    colors[3] = _map_gradient.Evaluate(0.2f);
                }

                _polygonBuilder.AddSquarePolygonWithColor(body_coords[x, y] * shape[x, y], body_coords[x + 1, y] * shape[x + 1, y], body_coords[x, y + 1] * shape[x, y + 1], body_coords[x + 1, y + 1] * shape[x + 1, y + 1], colors, bothSides);
            }
        }
    }

    /*!
        @fn public void setBodySize(int size)
        @brief �����, ��������������� ������ �������
        @details ������ _body_size ����� ��������
        @param size[IN] ����� ������ �������
    */
    public void setBodySize(int size)
    {
        _body_size = size;
    }

    /*!
        @fn public void setGradient(Gradient gradient)
        @brief �����, ��������������� �������� �������
        @details ������ _map_gradient ����� ��������
        @param gradient[IN] ����� �������� �������
    */
    public void setGradient(Gradient gradient)
    {
        _map_gradient = gradient;
    }

    /*!
        @fn public void CreatePlanet(int horizontal_len, int vertical_len, float [,] shape, bool color_map)
        @brief �����, ������������ �������
        @details ������� ������ mesh, ����� ������������ �����, � ����� ��� ������
        @param horizontal_len[IN] ���������� ������ �� �����������
        @param vertical_len[IN] ���������� ������ �� ���������
        @param float[,] shape[IN] ������ ������������� ������ ������ (������)
        @param bool color_map[IN] ��������, ���������� �� ��, ������������ �� �������� ��������
    */
    public void CreatePlanet(int horizontal_len, int vertical_len, float [,] shape, bool color_map)
    {
        ClearBody();
        MakeMesh(horizontal_len, vertical_len, shape, color_map);
        BuildBody();
    }
}
