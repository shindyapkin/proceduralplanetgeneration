using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

/*!
    @class Path_PanelHandler : MonoBehaviour
    @brief ������, ������� ������������ ���� � ����� �����
*/
public class Path_PanelHandler : MonoBehaviour
{
    /*!
        @var Slider InputField _edit
        @brief ��������� ����������, � ������� ������ ���� �������� �������������� InputField
    */
    public InputField _edit;

    /*!
        @var string _filePat
        @brief ����������, ������� ������ � ���� ���� � ������������ �����
    */
    string _filePath = "";

    /*!
        @fn public void findFile()
        @brief ����� ������������� ��� ������� �� ������
        @details ����� �������� ���� � ����������
    */
    public void findFile()
    {
        _filePath = EditorUtility.OpenFilePanel("Open file", "", "*.flac;*.wav");
        if (_filePath.Length != 0)
        {
            _edit.text = _filePath;
        } else
        {
            _edit.text = (string)"";
        }
    }

    /*!
        @fn public string getPath()
        @brief ����� ������������ ���� � ��������� �����
        @return ���������� ������� ���� � ����������
    */
    public string getPath()
    {
        return _filePath;
    }

}
